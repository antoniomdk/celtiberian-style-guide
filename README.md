# Celtiberian Style Guide

Coding style convention for most Celtiberian projects.

## Select your language

- [Javascript](./javascript/)

- [Python](./python/)
