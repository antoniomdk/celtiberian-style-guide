# Javascript Style Conventions

## Fundamentals

Here you have some basic knowledge about idiomatic Javascript
you need to know. Somo of the errors listed in the following guideline
cannot be catch by the linter. So, you need to be rigorous and write
well code along the linter.

[Idiomatic.js]()

## Setting up the linter

The linter we use is [Eslint](https://eslint.org/), you can install it with the following
command:

```
npm install -g eslint
```

## Setting up the code formatter

Most of the style errors can be fixed by using a code formatter. In this case
we recommend [prettier-eslint](https://www.npmjs.com/package/prettier-eslint) 
which formats the code according to your specific
eslint configuration. To install it, just run the following:

```
npm install -g prettier-eslint
```

## Configure VSCode extensions

If you use VSCode, you should install the these extensions:

- [Eslint Extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

- [Prettier Extension](https://github.com/prettier/prettier-vscode)

For the last one, set prettier.eslintIntegration to **true** in your VSCode preferences file.


## Forcing the style convention

In order to force the style among the developers in your team, you need to create a Git hook
that block a commit if any of the staged file does not follow the style rules.
To configure the git hook you need to install [Husky](https://github.com/typicode/husky)
and [lint-staged](https://github.com/okonet/lint-staged):

```
# In your project directory
npm install --save-dev husky
npm install --save-dev lint-staged
```

And create the Git hook by modifying the **package.json** of your project:

```json
{
  ...

  "dependencies": {
    ...
  },
  "devDependencies": {
    ...
  },

  ...

  "husky": {
    "hooks": {
      "pre-commit": "lint-staged"
    }
  },
  "lint-staged": {
    "*.{js,jsx}": [
      "eslint --fix",
      "git add"
    ]
  }

```

This hook will try to fix the code automatically but some errors have to be fixed manually.


## Eslint Configurations

The following configurations are grouped by strictness and technology, 
feel free to choose the most suitable to your project


### Level 1

For old projects with legacy code that should not be too much tweaked.

- [Javascript](./level1/)

### Level 2

For new to short life projects and people with little/no functional programming background.

- [Javascript](./level2/javascript): The same rules as Level 1 + some basic FP rules.
- [React](./level2/react): Javascript rules + Rules for PropTypes, Hooks, JSX, etc.
- [React Native](./level2/react): All the linting rules for React plus the React Native specific ones.

### Level 3

For new to short life projects and people with medium/strong functional programming background.

- [Javascript](./level3/javascript): The same rules as Level 2 + some advanced FP rules + Lodash/Ramda support.
- [React](./level3/react): Javascript rules + Rules for PropTypes, Hooks, JSX, etc.
- [React Native](./level3/react): All the linting rules for React plus the React Native specific ones.

## Miscellaneous

[Awesome Eslint](https://github.com/dustinspecker/awesome-eslint)

## TODO

- [ ] Add Spellchecking support
- [ ] Add rules for imports
- [ ] Limit cyclomatic and cognitive complexity
