module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'airbnb',
    'plugin:react/recommended',
    'plugin:fp/recommended',
    'plugin:react-native/all',
    'plugin:lodash/recommended',
    'plugin:ramda/recommended',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['fp', 'ramda', 'lodash'],
  rules: {
    'import/prefer-default-export': 'warn',
    'implicit-arrow-linebreak': 'off',
    'object-curly-spacing': ['warn', 'always'],
    'no-underscore-dangle': ['off', 'always'],
    // FP Plugin config config
    'fp/no-class': 'warn',
    'fp/no-loops': 'error',
    'fp/no-mutation': 'error',
    'fp/no-mutating-methods': 'warn',
    'fp/no-mutating-methods': [
      'warn',
      {
        allowedObjects: ['_', 'R', 'fp',],
      },
    ],
    'fp/no-nil': 'off',
    'fp/no-rest-parameters': 'off',
    'fp/no-unused-expression': 'off',
  },
};
