# Level 1

## How to use it

1. Install dependencies

```bash
npm install --save-dev eslint-config-airbnb
```

2. Copy the [.eslintrc](./.eslintrc) file into your project directory. 
