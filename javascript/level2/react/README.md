# Level 2 / React

## How to use it

1. Install dependencies

```bash
npm install --save-dev eslint-config-airbnb \
                       eslint eslint-plugin-fp \
                       eslint-plugin-react \
                       eslint-plugin-react-hooks
```

2. Copy the [.eslintrc](./.eslintrc) file into your project directory. 
