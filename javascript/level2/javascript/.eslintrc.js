module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'airbnb',
    'plugin:fp/recommended',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['fp'],
  rules: {
    'import/prefer-default-export': 'warn',
    'implicit-arrow-linebreak': 'off',
    'object-curly-spacing': ['warn', 'always'],
    // Allow to use underscore/lodash
    'no-underscore-dangle': ['off'],
    // FP Plugin config config
    'fp/no-class': 'warn',
    'fp/no-loops': 'warn',
    'fp/no-mutating-methods': [
      'warn',
      {
        allowedObjects: ['_', 'R', 'fp', 'Actions'],
      },
    ],
    'fp/no-nil': 'off',
    'fp/no-rest-parameters': 'off',
    'fp/no-unused-expression': 'off',
    'fp/no-mutation': 'off',
  },
};
